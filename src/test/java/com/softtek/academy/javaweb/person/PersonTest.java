package com.softtek.academy.javaweb.person;


import static org.junit.Assert.assertSame;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.softtek.academy.javaweb.configuration.JDBCConfiguration;
import com.softtek.academy.javaweb.repository.PersonRepository;
import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.services.PersonService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JDBCConfiguration.class })
@WebAppConfiguration
public class PersonTest {

	@Autowired
	private PersonService personService;
	@Autowired
	private PersonRepository repo;
	
	@Test
	public void getPersonsTestUnit() {
		List<Person> persons= repo.getAll();
		int actual = persons.get(0).getId();
		int expected = 1;
		assertSame(expected, actual);
	}
	
	@Test
	public void getPersonsTest() {
		List<Person> persons = personService.getAll();
		int actual = persons.get(0).getId();
		int expected = 1;
		assertSame(expected, actual);
	}

}
