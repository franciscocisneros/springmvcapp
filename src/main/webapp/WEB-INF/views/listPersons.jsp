<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
         <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h4>Personas</h4>
<table border="1">
<tr>
	<th>ID</th>
	<th>Nombre</th>
	<th>Edad</th>
</tr>
<tr>
<c:forEach items="${list}" var="list">
 <td> <h4>${list.id}</h4></td>
  <td>${list.name}</td>    
 <td>${list.age}</td> 
 <td><a href ="http://localhost:8080/SpringMVCApp/editemap/${list.id}"> Modificar </a></td>
 <td><a href = "http://localhost:8080/SpringMVCApp/deletePerson/${list.id}"> Eliminar </a></td>
 </tr>             
</c:forEach>

</table>
<a href = "http://localhost:8080/SpringMVCApp/person"> Nueva persona</a>
</body>
</html>