package com.softtek.academy.javaweb.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.softtek.academy.javaweb.dao.PersonDAO;
import com.softtek.academy.javaweb.beans.Person;

@Service
@Transactional
public class PersonServiceImpl implements PersonService{

	@Autowired
	@Qualifier("personRepository")
	private PersonDAO personDAO;

	@Override
	public List<Person> getAll() {
		return personDAO.getAll();
	}

	@Override
	public Person getById(int id) {
		return personDAO.getById(id);
	}

	@Override
	public int deletePerson(int id) {
		return personDAO.deletePerson(id);
	}

	@Override
	public int newPerson(int id, String name, int age) {
		return personDAO.newPerson(id, name, age);
		
	}

	@Override
	public Person updatePerson(Person s) {
		return personDAO.updatePerson(s);
	}
}
