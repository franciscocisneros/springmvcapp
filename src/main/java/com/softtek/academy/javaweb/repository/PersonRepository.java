package com.softtek.academy.javaweb.repository;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.dao.PersonDAO;

@Repository("personRepository")
public class PersonRepository implements PersonDAO{

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<Person> getAll() {
		return (List<Person>)entityManager
				.createQuery("Select s from Person s", Person.class).getResultList();
	}

	@Override
	public Person getById(int id) {
		return entityManager.find(Person.class,id);
	}

	@Override
	public int deletePerson(int id) {
		entityManager.remove(entityManager.find(Person.class, id));		
		return 0;
	}

	@Override
	public int newPerson(int id, String name, int age) {
		Person person = new Person();
		person.setAge(age);
		person.setId(id);
		person.setName(name);
		entityManager.persist(person);
		return 0;
		
	}

	@Override
	public Person updatePerson(Person s) {
		Person actual = entityManager.find(Person.class, s.getId());
		actual.setAge(s.getAge());
		actual.setName(s.getName());
		entityManager.merge(actual);
		return s;
	}

}
