package com.softtek.academy.javaweb.dao;

import java.util.List;

import com.softtek.academy.javaweb.beans.Person;

public interface PersonDAO {
	List<Person> getAll();
	
	Person getById(int id);
	
	int deletePerson(int id);
	
	int newPerson(int id, String name, int age);
	
	Person updatePerson(Person s);
}