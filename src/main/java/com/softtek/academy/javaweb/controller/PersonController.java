package com.softtek.academy.javaweb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.services.PersonService;


@Controller
public class PersonController {
	@Autowired
	PersonService personService;
	
	
	@RequestMapping (value = "/person", method = RequestMethod.GET)
	public ModelAndView person() {
		return new ModelAndView ("person", "command", new Person());
	}
	
	
	 @RequestMapping(value = "/addPerson", method = RequestMethod.POST)
	    public String addPerson(@ModelAttribute("SpringWeb")Person person, ModelMap model) {
	        model.addAttribute("name", person.getName());
	        model.addAttribute("age", person.getAge());
	        model.addAttribute("id", person.getId());
	        List<Person> list = personService.getAll();  
	        model.addAttribute("list", list);
	        personService.newPerson(person.getId(), person.getName(), person.getAge());
		
	        return "newPerson";
	    }
	 
	 @RequestMapping(value = "/deletePerson/{id}", method = RequestMethod.GET)
	 	public String deletePerson(@PathVariable int id) {
		 personService.deletePerson(id);
			return "redirect:/listPerson2";
	 }
	 @RequestMapping("/listPerson2")    
	    public String listPerson2(Model model){    
		    List<Person> list = personService.getAll();
	        model.addAttribute("list", list);
	        return "listPersons";    
	    }  
	 @RequestMapping(value="/editemap/{id}")    
	    public String edit(@PathVariable int id, ModelMap m){ 
		    Person stdao = personService.getById(id);
	        m.addAttribute("command",stdao);  
	        return "editPerson";    
	    }  
	 @RequestMapping(value="/editsave",method = RequestMethod.POST)    
	    public String editsave(@ModelAttribute("emp") Person emp){   
		 personService.updatePerson(emp);  
	        return "redirect:/listPerson2";    
	    }    
}
